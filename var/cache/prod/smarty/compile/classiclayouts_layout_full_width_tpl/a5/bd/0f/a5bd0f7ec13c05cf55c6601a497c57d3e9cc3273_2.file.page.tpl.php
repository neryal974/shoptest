<?php
/* Smarty version 3.1.39, created on 2021-08-29 18:00:43
  from 'C:\wamp64\www\shoptest\themes\classic\templates\page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_612baf2b6904e4_98119069',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a5bd0f7ec13c05cf55c6601a497c57d3e9cc3273' => 
    array (
      0 => 'C:\\wamp64\\www\\shoptest\\themes\\classic\\templates\\page.tpl',
      1 => 1629979756,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_612baf2b6904e4_98119069 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_297705690612baf2b675fa0_42753543', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_1373056609612baf2b679c18_78386628 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
          <h1><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_585020568612baf2b677a76_87753558 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1373056609612baf2b679c18_78386628', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_1390436016612baf2b683d18_81336845 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_393754750612baf2b686458_88749886 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_1134447416612baf2b682252_62339636 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1390436016612baf2b683d18_81336845', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_393754750612baf2b686458_88749886', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_1007603239612baf2b68bdf6_05075034 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_1068851333612baf2b68a367_60542894 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1007603239612baf2b68bdf6_05075034', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_297705690612baf2b675fa0_42753543 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_297705690612baf2b675fa0_42753543',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_585020568612baf2b677a76_87753558',
  ),
  'page_title' => 
  array (
    0 => 'Block_1373056609612baf2b679c18_78386628',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_1134447416612baf2b682252_62339636',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_1390436016612baf2b683d18_81336845',
  ),
  'page_content' => 
  array (
    0 => 'Block_393754750612baf2b686458_88749886',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_1068851333612baf2b68a367_60542894',
  ),
  'page_footer' => 
  array (
    0 => 'Block_1007603239612baf2b68bdf6_05075034',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_585020568612baf2b677a76_87753558', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1134447416612baf2b682252_62339636', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1068851333612baf2b68a367_60542894', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
