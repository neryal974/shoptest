<?php
/* Smarty version 3.1.39, created on 2021-08-29 18:03:01
  from 'module:psfeaturedproductsviewste' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_612bafb58efd71_13021225',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa6cc378d2942c8857b89d6bca728048c0caeedd' => 
    array (
      0 => 'module:psfeaturedproductsviewste',
      1 => 1629979756,
      2 => 'module',
    ),
    '2e86eceed61ea06b80f25404bcea3046b895add3' => 
    array (
      0 => 'C:\\wamp64\\www\\shoptest\\themes\\classic\\templates\\catalog\\_partials\\productlist.tpl',
      1 => 1629979756,
      2 => 'file',
    ),
    '9cd459ea369bae01079ea9e1756ac886c64ede4d' => 
    array (
      0 => 'C:\\wamp64\\www\\shoptest\\themes\\classic\\templates\\catalog\\_partials\\miniatures\\product.tpl',
      1 => 1629979756,
      2 => 'file',
    ),
    '138c8345557d661f9d7461e46616191a012877b7' => 
    array (
      0 => 'C:\\wamp64\\www\\shoptest\\themes\\classic\\templates\\catalog\\_partials\\product-flags.tpl',
      1 => 1629979756,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_612bafb58efd71_13021225 (Smarty_Internal_Template $_smarty_tpl) {
?><section class="featured-products clearfix">
  <h2 class="h2 products-section-title text-uppercase">
    Produits populaires
  </h2>
  <div class="products row" itemscope itemtype="http://schema.org/ItemList">
            
<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="product">
  <meta itemprop="position" content="0" />  <article class="product-miniature js-product-miniature" data-id-product="2" data-id-product-attribute="0" itemprop="item" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost/shoptest/petite-perruche/2-perruche-ondule.html" class="thumbnail product-thumbnail">
            <img
              src="http://localhost/shoptest/2-home_default/perruche-ondule.jpg"
              alt="perruche ondulé"
              data-full-size-image-url="http://localhost/shoptest/2-large_default/perruche-ondule.jpg"
              />
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://localhost/shoptest/petite-perruche/2-perruche-ondule.html" itemprop="url" content="http://localhost/shoptest/petite-perruche/2-perruche-ondule.html">perruche ondulé</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Prix">20,00 €</span>
              <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="invisible">
                <meta itemprop="priceCurrency" content="EUR" />
                <meta itemprop="price" content="20" />
              </div>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="2" data-url="http://localhost/shoptest/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags">
                    <li class="product-flag new">Neuf</li>
            </ul>


      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
          </a>
        

        
                  
      </div>
    </div>
  </article>
</div>

            
<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="product">
  <meta itemprop="position" content="1" />  <article class="product-miniature js-product-miniature" data-id-product="7" data-id-product-attribute="0" itemprop="item" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost/shoptest/perruche-de-taille-moyenne/7-grand-alexandre.html" class="thumbnail product-thumbnail">
            <img
              src="http://localhost/shoptest/7-home_default/grand-alexandre.jpg"
              alt="Grand Alexandre"
              data-full-size-image-url="http://localhost/shoptest/7-large_default/grand-alexandre.jpg"
              />
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://localhost/shoptest/perruche-de-taille-moyenne/7-grand-alexandre.html" itemprop="url" content="http://localhost/shoptest/perruche-de-taille-moyenne/7-grand-alexandre.html">Grand Alexandre</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Prix">150,00 €</span>
              <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="invisible">
                <meta itemprop="priceCurrency" content="EUR" />
                <meta itemprop="price" content="150" />
              </div>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="7" data-url="http://localhost/shoptest/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags">
                    <li class="product-flag new">Neuf</li>
            </ul>


      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
          </a>
        

        
                  
      </div>
    </div>
  </article>
</div>

            
<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="product">
  <meta itemprop="position" content="2" />  <article class="product-miniature js-product-miniature" data-id-product="8" data-id-product-attribute="0" itemprop="item" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://localhost/shoptest/accueil/8-l-ara-ararauna.html" class="thumbnail product-thumbnail">
            <img
              src="http://localhost/shoptest/8-home_default/l-ara-ararauna.jpg"
              alt="L&#039;ara ararauna"
              data-full-size-image-url="http://localhost/shoptest/8-large_default/l-ara-ararauna.jpg"
              />
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://localhost/shoptest/accueil/8-l-ara-ararauna.html" itemprop="url" content="http://localhost/shoptest/accueil/8-l-ara-ararauna.html">L&#039;ara ararauna</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Prix">1 500,00 €</span>
              <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="invisible">
                <meta itemprop="priceCurrency" content="EUR" />
                <meta itemprop="price" content="1500" />
              </div>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="8" data-url="http://localhost/shoptest/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags">
                    <li class="product-flag new">Neuf</li>
            </ul>


      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
          </a>
        

        
                  
      </div>
    </div>
  </article>
</div>

    </div>  <a class="all-product-link float-xs-left float-md-right h4" href="http://localhost/shoptest/2-accueil">
    Tous les produits<i class="material-icons">&#xE315;</i>
  </a>
</section>
<?php }
}
