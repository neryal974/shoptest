<?php
/* Smarty version 3.1.39, created on 2021-08-29 18:03:01
  from 'module:psimagesliderviewstemplat' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_612bafb56e2621_65485063',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6c2108a17c7103b6e203f4f0621d4645b56b0114' => 
    array (
      0 => 'module:psimagesliderviewstemplat',
      1 => 1629979756,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_612bafb56e2621_65485063 (Smarty_Internal_Template $_smarty_tpl) {
?>
  <div id="carousel" data-ride="carousel" class="carousel slide" data-interval="5000" data-wrap="true" data-pause="hover" data-touch="true">
    <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
            <li data-target="#carousel" data-slide-to="2"></li>
          </ol>
    <ul class="carousel-inner" role="listbox">
              <li class="carousel-item active" role="option" aria-hidden="false">
          <a href="http://www.prestashop.com/?utm_source=back-office&amp;utm_medium=v17_homeslider&amp;utm_campaign=back-office-FR&amp;utm_content=download">
            <figure>
              <img src="http://localhost/shoptest/modules/ps_imageslider/images/3cc97ed2b206f8ce90422ecbbe907c9be7c01d22_carrousel-1.png" alt="sample-1">
                              <figcaption class="caption">
                  <h2 class="display-1 text-uppercase">Les petites perruche</h2>
                  <div class="caption-description"><h3>vous recherchez une petite perruche </h3>
<p> (perruche ondulé, La perruche Calopsitte, la perruche splendide , l'inséparable et bien d'autres encore !)</p></div>
                </figcaption>
                          </figure>
          </a>
        </li>
              <li class="carousel-item " role="option" aria-hidden="true">
          <a href="http://www.prestashop.com/?utm_source=back-office&amp;utm_medium=v17_homeslider&amp;utm_campaign=back-office-FR&amp;utm_content=download">
            <figure>
              <img src="http://localhost/shoptest/modules/ps_imageslider/images/9145070d13ad9bca78f6aa65095815e0595ae8af_carrousel-1PC.png" alt="sample-2">
                              <figcaption class="caption">
                  <h2 class="display-1 text-uppercase">Les perruche moyenne </h2>
                  <div class="caption-description"><h3>vous recherchez une perruche de taille moyenne </h3>
<p>Perruche colier, perruche soleil , perruche à ailes vertes, perruche Alexandre,  </p></div>
                </figcaption>
                          </figure>
          </a>
        </li>
              <li class="carousel-item " role="option" aria-hidden="true">
          <a href="http://www.prestashop.com/?utm_source=back-office&amp;utm_medium=v17_homeslider&amp;utm_campaign=back-office-FR&amp;utm_content=download">
            <figure>
              <img src="http://localhost/shoptest/modules/ps_imageslider/images/6e4360db2fe7409fdfb3c1fa1ab703ad352c120c_carrousel-1cc.png" alt="sample-3">
                              <figcaption class="caption">
                  <h2 class="display-1 text-uppercase">Les grandes perruches et perroquets </h2>
                  <div class="caption-description"><h3>vous recherchez de grande perruche, Perroquets </h3>
<p>(gris du Gabon, Cacatoès, hiboux)</p>
<h1 id="firstHeading" class="firstHeading" style="margin:0px 0px .25em;padding:0px;border-bottom:1px solid rgb(162,169,177);font-size:1.8em;font-weight:normal;font-family:'Linux Libertine', Georgia, Times, serif;line-height:1.3;background-color:rgb(255,255,255);"></h1></div>
                </figcaption>
                          </figure>
          </a>
        </li>
          </ul>
    <div class="direction" aria-label="Boutons du carrousel">
      <a class="left carousel-control" href="#carousel" role="button" data-slide="prev" aria-label="Précédent">
        <span class="icon-prev hidden-xs" aria-hidden="true">
          <i class="material-icons">&#xE5CB;</i>
        </span>
      </a>
      <a class="right carousel-control" href="#carousel" role="button" data-slide="next" aria-label="Suivant">
        <span class="icon-next" aria-hidden="true">
          <i class="material-icons">&#xE5CC;</i>
        </span>
      </a>
    </div>
  </div>
<?php }
}
